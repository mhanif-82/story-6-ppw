from django import forms
from Status.models import StatusModel
from datetime import datetime


class StatusForm(forms.ModelForm):
    class Meta:
        model = StatusModel
        fields= ('status_desc',)
        widgets= {
            'status_desc': forms.Textarea(attrs={'class' : 'form-control'}),
        }