from django.test import TestCase
from django.test import Client
from Status.views import index
from Status.models import StatusModel
from .views import index
from selenium import webdriver
from selenium.webdriver.common.keys import Keys 
from selenium.webdriver.chrome.options import Options
import time
import unittest
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse
from project.settings import BASE_DIR
import os

class TestProjectFunctional (StaticLiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome(executable_path=os.path.join(BASE_DIR,"chromedriver"),chrome_options=chrome_options)
        super(TestProjectFunctional,self).setUp()
        print("Browser On")

    def tearDown(self):
        self.selenium.quit()
        super(TestProjectFunctional,self).tearDown()
        print("browser off")

    def test_opening_browser_user_see_title(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/")
        self.assertIn("Status",selenium.title)
        print("Judulnya Sesuai")

    def test_user_sees_header(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/")
        header = selenium.find_element_by_tag_name('h1').text
        self.assertEquals('Halo,Apa Kabar?', header)
        print("User Melihat Header")

    def test_inputting_status(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/")
        message = "Test"
        self.assertNotIn(message,selenium.page_source)
        text_area = selenium.find_element_by_name("status_desc")
        text_area.send_keys(message)
        sub_button = selenium.find_element_by_id("postbutton")
        sub_button.click()
        self.assertIn(message,selenium.page_source)
        print("Kata Test Muncul di Body")

    # 1 rem = 16px
    def test_ukuran_header(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/")
        text = selenium.find_element_by_name("headerutama").value_of_css_property("font-size")
        self.assertEquals('40px',text)
        print("Ukuran Judul sesuai")

    def test_ada_button_post(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/")
        tombol = "<button"
        self.assertIn(tombol,selenium.page_source)
        print("ada tombol post")
    
    def test_ada_input_post(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/")
        kolom_status = "<textarea"
        self.assertIn(kolom_status,selenium.page_source)
        print("ada kolom status")

class TestStatus(TestCase):

    def test_status_is_exist(self):
        self.client = Client()
        response = self.client.get('')
        self.assertEqual(response.status_code,200)

    def test_statustemplate_is_correct (self):
        self.client = Client()
        response = self.client.get('')
        self.assertTemplateUsed(response, 'status.html')

    def test_form(self):
        data = {
            'status_desc' : "Nyante",
            'waktu' : "2019-01-01 00:00:00",
        }
        response = Client().post("", data)
        self.assertEqual(response.status_code, 302)
    
    def test_model_status(self):
        status_test = StatusModel(
            status_desc = "Nyante",
            waktu = "2019-01-01 00:00:00",
        ) 
        status_test.save()
        self.assertEqual(StatusModel.objects.all().count(), 1)


