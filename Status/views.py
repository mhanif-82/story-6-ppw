from django.shortcuts import render,redirect
from Status.forms import StatusForm
from Status.models import StatusModel

# Create your views here.
def index (request): 
    status_all = StatusModel.objects.all().order_by('-waktu')
    data = StatusForm(request.POST)
    if request.method == "POST":
        if data.is_valid():
            saved = data.save()
            return redirect("/") 
    
    return render(request, "status.html", {"form": StatusForm(), "semua_status": status_all } )