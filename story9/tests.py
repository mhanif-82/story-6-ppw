from django.test import TestCase, Client
from django.urls import resolve
from django.contrib.staticfiles.storage import staticfiles_storage 
from django.contrib.staticfiles import finders
from django.contrib.auth.models import User
from .views import loginpage, logoutpage, signuppage
from .forms import Formsignup, Formlogin
from django.http import HttpRequest
import time
# Create your tests here.
class story9unitTests(TestCase):
    def test_story9login_url_is_resolved(self):
       r = Client().get('/story9/login/')
       self.assertEqual(r.status_code, 200)

    def test_story9signup_url_is_resolved(self):
       r = Client().get('/story9/signup/')
       self.assertEqual(r.status_code, 200)

    def test_story9login_template_are_correct(self):
        response = Client().get('/story9/login/')
        self.assertTemplateUsed(response, 'loginpage.html')

    def test_story9signup_template_are_correct(self):
        response = Client().get('/story9/signup/')
        self.assertTemplateUsed(response, 'signuppage.html')

    def test_using_about_func_story9login(self):
        found = resolve('/story9/login/')
        self.assertEqual(found.func, loginpage)

    def test_using_about_func_story9signup(self):
        found = resolve('/story9/signup/')
        self.assertEqual(found.func, signuppage)
        
    def test_using_about_func_story9logout(self):
        found = resolve('/story9/logout/')
        self.assertEqual(found.func, logoutpage)

    def test_html_element_login_exist(self):
        request = HttpRequest()
        response = loginpage(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Sign In', html_response)

    def setUp(self):
        u = User.objects.create(username="iniunittest")
        u.set_password("ppwauthetication")
        u.save()
    
    def test_user_object(self):
        user_test = User.objects.get(username="iniunittest")
        self.assertEquals(user_test.username, 'iniunittest')

    def test_forms_login_valid(self):
        c = Client()
        response = c.post('/story9/login/',username="iniunittest", password="ppwauthentication")
        self.assertTrue(response)
        html_response = response.content.decode('utf8')
        self.assertIn('username', html_response)

    def test_forms_login_not_valid(self):
        form_data = {'username': 'iniunittest'}
        form = Formlogin(data=form_data)
        self.assertFalse(form.is_valid())

    def test_forms_signup_valid(self):
        form_data = {'username': 'unittestdua', 'password1':'ppwauthentication', 'password2':'ppwauthentication'}
        form = Formsignup(data=form_data)
        self.assertTrue(form.is_valid())

    def test_response_for_signup_valid(self):
        c = Client()
        response = c.post('/story9/signup/',username="iniunittest", password1="ppwauthentication", password2="ppwauthentication")
        self.assertTrue(response)

    def test_forms_signup_is_not_valid(self):
        form_data = {'username': 'unittestdua'}
        form = Formsignup(data=form_data)
        self.assertFalse(form.is_valid())
