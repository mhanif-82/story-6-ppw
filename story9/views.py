from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout, authenticate
from .forms import Formlogin, Formsignup
from django.contrib import messages
from django.contrib.auth.models import User
# Create your views here.
def loginpage(request):
    context = { 
        'greeting' : 'Welcome to Our Website',
        'please' : 'Please Log In or Sign Up First',
        'log' : 'login',
    }

    if request.method == "POST":
        formie = Formlogin(request, data=request.POST)
        if formie.is_valid():
            username = formie.cleaned_data.get('username')
            password = formie.cleaned_data.get('password')
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                request.session['greeting'] = 'Welcomeback ' + request.user.username
                request.session['please'] = 'Please Log Out'

        else:
            messages.info(request, 'Wrong password/username')
        return render(request, 'loginpage.html', {'formie' : formie})
    
    else:
        formie = Formlogin()
        return render(request, 'loginpage.html', {'formie' : formie})


def signuppage(request):
    if request.method == "POST":
        formie = Formsignup(request.POST)
        if formie.is_valid():
            user = formie.save()
            login(request, user)
            request.session['greeting'] = 'Welcomeback ' + request.user.username
            request.session['please'] = 'Please Log Out'
            return redirect("login")
        else:
            messages.info(request, 'Either your entry is not valid or username is already taken. Please try again')
        return render(request, 'signuppage.html', {'formie' : formie})
    else:
        formie = Formsignup()
        return render(request, 'signuppage.html', {'formie' : formie})

def logoutpage(request):
    logout(request)
    return redirect("login")
    request.session.flush()

    