from django.contrib import admin
from django.urls import path, include
from story8.views import index

app_name = 'story8'
urlpatterns = [
    path('', index, name="index"),
]
