from django.test import TestCase, Client
from django.urls import resolve
from story8.views import index

# Create your tests here.
class Story8Test(TestCase):
    # testing whether the url exist or not
    def test_landing_url_is_exist(self):
        response = Client().get('/story8/')  # calling router if browser get story 9
        # 200 is the response if not calling error 404
        self.assertEqual(response.status_code, 200)

    def test_story_using_correct_template(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'story8.html')

    def test_story_using_index_function(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, index)

    def test_story_using_populate_function(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, index)

