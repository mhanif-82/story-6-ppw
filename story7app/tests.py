from django.test import TestCase, Client
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.contrib.staticfiles.storage import staticfiles_storage 
from django.contrib.staticfiles import finders
from .views import story7app_page
from django.http import HttpRequest
import time
# Create your tests here.
class story7UnitTests(TestCase):
    def test_story7_url_is_resolved(self):
       r = Client().get('/story7/')
       self.assertEqual(r.status_code, 200)

    def test_landingpage_template_are_correct(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response, 'story7.html')

    def test_using_about_func(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, story7app_page)
        
    def test_heading_and_button_exist(self):
        request = HttpRequest()
        response = story7app_page(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Hello World', html_response)
        self.assertIn('Dark Mode', html_response)

class story7FunctionalTests(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.get('http://127.0.0.1:8000/story7')
        super(story7FunctionalTests, self).setUp()

    def test_accordion(self):
        selenium = self.selenium
        # selenium.get('http://127.0.0.1:8000/story7')
          
        accordion_test = selenium.find_element_by_id('ui-id-5').text
        self.assertEqual(accordion_test, 'Achievement')

    def test_theme_change_is_working(self):
        selenium = self.selenium
        # selenium.get('http://127.0.0.1:8000/story7')
        # time.sleep(5)
        button_light = selenium.find_element_by_name("lightmode")
        # time.sleep(3)
        button_light.click()
        # time.sleep(3)
        background = selenium.find_element_by_tag_name("body").value_of_css_property('background-color')
        self.assertIn('rgba(255, 255, 255, 1)', background)

    def tearDown(self):
        self.selenium.quit()
        super(story7FunctionalTests, self).tearDown()


