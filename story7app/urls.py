from django.urls import path
from . import views

urlpatterns = [
    path('', views.story7app_page, name="story7app"),
]